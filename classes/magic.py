import random


class Spell:
    def __init__(self, name, cost, dmg, type):
        self.name = name
        self.cost = cost
        self.dmg = dmg
        self.type = type

    def generate_damage(self):
        dmgl = self.dmg - 5
        dmgh = self.dmg + 5
        return random.randrange(dmgl, dmgh)
