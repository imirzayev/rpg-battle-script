import random


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class Person:
    def __init__(self, name, hp, mp, atk, df, magic, item):
        self.name = name
        self.maxhp = hp
        self.hp = hp
        self.maxmp = mp
        self.mp = mp
        self.atkl = atk - 10
        self.atkh = atk + 10
        self.df = df
        self.magic = magic
        self.item = item
        self.actions = ["Attack", "Magic", "Items"]

    def generate_damage(self):
        return random.randrange(self.atkl, self.atkh)

    def take_damage(self, dmg):
        self.hp = self.hp - dmg
        if self.hp < 0:
            self.hp = 0
        return self.hp

    def heal(self, hl):
        self.hp += hl
        if self.hp > self.maxhp:
            self.hp = self.maxhp

    def get_hp(self):
        return self.hp

    def get_max_hp(self):
        return self.maxhp

    def get_mp(self):
        return self.mp

    def get_max_mp(self):
        return self.maxmp

    def reduce_mp(self, cost):
        self.mp = self.mp - cost

    def choose_action(self):
        i = 1
        print(bcolors.OKBLUE + bcolors.BOLD + "    Actions" + bcolors.ENDC)
        for item in self.actions:
            print("        " + str(i) + ".", item)
            i += 1

    def choose_enemy(self, enemies):
        i = 1
        print(bcolors.FAIL + bcolors.BOLD + "    Enemies" + bcolors.ENDC)
        for enemy in enemies:
            print("        " + str(i) + ".", enemy.name.replace(" ", ""))
            i += 1
        enemy_index = int(input("    Choose Enemy: ")) - 1
        return enemies[enemy_index]

    def choose_magic(self):
        i = 1
        print(bcolors.OKBLUE + bcolors.BOLD + "    Magic" + bcolors.ENDC)
        for spell in self.magic:
            print("        " + str(i) + ".", spell.name, "(cost: ", str(spell.cost) + ")")
            i += 1

    def choose_item(self):
        i = 1
        print(bcolors.OKBLUE + bcolors.BOLD + "    Item" + bcolors.ENDC)
        for item in self.item:
            print("        " + str(i) + ".", item["item"].name, ":", str(item["item"].description) +
                  " (x" + str(item["quantity"]) + ")")
            i += 1

    def choose_enemy_spell(self):
        spell_choice = random.randrange(0, len(self.magic))
        enemy_spell = self.magic[spell_choice]
        pct = self.get_hp() / self.get_max_hp() * 100

        # if enemy_spell.type == "white" and pct > 50:
        #     self.choose_enemy_spell()
        # else:
        return enemy_spell


    def get_enemy_stats(self):
        cur_hp = self.make_numbers(11, "hp")
        hp_bar = self.make_bar(50, "hp")

        print("                           __________________________________________________")
        print(self.name + "         " + bcolors.BOLD +
              cur_hp + "|" +
              bcolors.FAIL + hp_bar +
              bcolors.ENDC + "|")

    def get_stats(self):
        cur_hp = self.make_numbers(7, "hp")
        cur_mp = self.make_numbers(5, "mp")
        hp_bar = self.make_bar(25, "hp")
        mp_bar = self.make_bar(10, "mp")

        print("                           _________________________               __________")
        print(self.name + "              " + bcolors.BOLD +
              cur_hp + "|" +
              bcolors.OKGREEN + hp_bar +
              bcolors.ENDC + "|        " + cur_mp + "|" + bcolors.OKBLUE +
              mp_bar + bcolors.ENDC + "|")

    def make_bar(self, grid, grid_type):
        bar = ""

        if grid_type == "hp":
            ticks = self.hp / self.maxhp * grid
        elif grid_type == "mp":
            ticks = self.mp / self.maxmp * grid

        while ticks > 0:
            bar += "█"
            ticks -= 1

        while len(bar) < grid:
            bar += " "
        return bar

    def make_numbers(self, p_digits, grid_type):
        if grid_type == "mp":
            string = str(self.mp) + "/" + str(self.maxmp)
            cur_p = ""
        elif grid_type == "hp":
            string = str(self.hp) + "/" + str(self.maxhp)
            cur_p = ""

        if len(string) < p_digits:
            decreased = p_digits - len(string)
            while decreased > 0:
                cur_p += " "
                decreased -= 1
            cur_p += string
        else:
            cur_p = string

        return cur_p



